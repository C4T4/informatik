package de.ostfalia.gdp.ws16.s3;

import java.util.Scanner;

public class AbessinischeBauernregel {
	public static void main (String [] args) {
		Scanner sc = new Scanner(System.in);
		int a, a1, b, b1, erg = 0;
		System.out.println("Zahl 1");
		a = sc.nextInt();
		System.out.println("Zahl 2");
		b = sc.nextInt();
		String S1 = ("*****************\n"
				+ "*   Schritt 1   *\n"
				+ "*****************");

		String S2 = ("*****************\n"
				+ "* Schritt 2 & 3 *\n"
				+ "*****************");
		
		String S4 = ("*****************\n"
				+ "*   Schritt 4   *\n"
				+ "*****************");
		
		String S5 = ("*****************\n"
				+ "*   Schritt 5   *\n"
				+ "*****************");
				
		
		
		
		
		/**
		 * Schritt 1
		 */
		System.out.println(S1);
		System.out.println(a + " x " + b);
		
		
		/**
		 * Schritt 2 & 3
		 */
		System.out.println(S2);
		a1 = a;
		b1 = b;
		
		do {
			a1 = a1 / 2;
			b1 = b1 * 2;
			System.out.println(a1 + " x " + b1);
		} while (a1 != 1);
		
		
		/**
		 * Schritt 4
		 */
		System.out.println(S4);
		do {

			if (!(a % 2 == 0)) {
				System.out.println(a + " x " + b);
				erg = erg + b;
			}
			a = a / 2;
			b = b * 2;
		} while (a != 0);
		
		
		/**
		 * Schritt 5
		 */
		System.out.println(S5);
		System.out.println("Ergebnis: " + erg);
		sc.close();
	}
}
