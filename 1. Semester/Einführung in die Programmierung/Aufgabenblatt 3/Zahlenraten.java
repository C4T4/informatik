package de.ostfalia.gdp.ws16.s3;

import java.util.Scanner;

public class Zahlenraten {
	public static void main (String[] args) {
		Scanner sc = new Scanner (System.in);
		int sp, z = 1;
		int pc =  (int) (199 * Math.random() + 1);
		System.out.println("Hallo, ich denk mir eine Zahl zwischen 1 und 200. Rate mal!");
		
		do {
			System.out.println(z + ". Versuch");
			sp = sc.nextInt();
			if (sp > pc) {
				System.out.println("Meine Zahl ist kleiner");
			} else if (sp < pc) {
				System.out.println("Meine Zahl ist größer");
			}
			z++;
		} while (sp != pc);
		
		System.out.println("Meine Zahl ist " + pc + "und du hast " + z + " Versuche benötigt");	
		sc.close();
	
	}

}
