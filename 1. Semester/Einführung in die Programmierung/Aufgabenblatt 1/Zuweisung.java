package de.ostfalia.gdp.ws16.s1;

public class Zuweisung {
		public static void main(String[] ignored) {
			int number = 1;
			System.out.println(number);
			int zero = 0;
			System.out.println(zero);
			number = 2 * zero;
			System.out.println(number);
			zero = 42;
			System.out.println(number);
			number = number + 2;
			System.out.println(number);
			number = number + number;
			System.out.println(number);
			number = number + zero;
			System.out.println(zero);
		}

}
