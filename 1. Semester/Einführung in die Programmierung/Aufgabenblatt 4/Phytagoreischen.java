package de.ostfalia.gdp.ws16.s4;

public class Phytagoreischen {
	public static void main (String[] args) {
		for (int a = 1; a <= 100; a++) {
			for (int b = 1; b <= a; b++) {
				for (int c = 1; c <= b; c++) {
					if ((c * c) + (b * b) == (a*a)) {
						System.out.println(c + " + " + b + " = " + a);
					}
					
				}
			}
		}
	}

}
