package de.ostfalia.gdp.ws16.s4;

import java.util.Scanner;

public class LetterSort {
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		String in = "";
		String ou = ""; 
		int uc_offset = 0x41;
		int lc_offset = 0x61;
		in = sc.nextLine();
		
		for (char i = 0; i < 26; i++) {
			for (char j = 0; j < in.length(); j++) {
				char curr = in.charAt(j);
				
				if (curr == (i+'A') || curr == (i+'a')) {
					ou += curr;
				} 
			}
		}
		System.out.println(ou);
		sc.close();
	}

}
