package de.ostfalia.gdp.ws15.s2;

import java.util.Scanner;

public class LogicCircuit {
	public static void main (String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Geben Sie bitte die vier Eingänge an: ");
		boolean a = sc.nextBoolean();
		boolean b = sc.nextBoolean();
		boolean c = sc.nextBoolean();
		boolean d = sc.nextBoolean();
		
		
		boolean erg = false;
		
		if (a == false) {
			if (b == false) {
				if (c == true && d == true) {
					erg = true;
				}
			} else if (b == true) {
				if (c == true && d == false) {
					erg = true;
				} else if (c == false && d == true) {
					erg = true;
				}
			}
		} else if (a == true) {
			if (b == false) {
				if (c == true && d == false) {
					erg = true;
				} else if (c == false && d == true) {
					erg = true;
				}
			} else if (b == true) {
				if (c == false && d == false) {
					erg = true;
				}
			}
		}
		
		System.out.println(erg);
		sc.close();
		
	}

}
