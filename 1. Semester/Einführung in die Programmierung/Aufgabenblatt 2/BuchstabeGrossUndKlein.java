package de.ostfalia.gdp.ws15.s2;

import java.io.UnsupportedEncodingException;
import java.util.Scanner;

public class BuchstabeGrossUndKlein {
	public static void main(String[] args) throws UnsupportedEncodingException {
		Scanner sc = new Scanner(System.in);
		int c = 0;
		System.out.print("Buchstabe eingeben: ");
		c = (int)sc.next().charAt(0);
		
		if (!((0x41 <= c && c <= 0x54 )||(0x61 <= c && c <= 0x7a))) {
			sc.close();
			return;
		}
		c ^= 0x20;
		System.out.println((char)c);
		sc.close();
	}	
}