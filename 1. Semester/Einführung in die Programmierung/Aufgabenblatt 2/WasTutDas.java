package de.ostfalia.gdp.ws15.s2;

import java.util.Scanner;

public class WasTutDas {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double a, b, c, d, e;
		a = sc.nextDouble();
		b = sc.nextDouble();
		c = sc.nextDouble();
		d = sc.nextDouble();
		
		if (b > a && c > b) {
			if (d > c) {
				e = d;
			} else {
				e = c;
			}
		} else if (b > a && !(c > b)) {
			if (d > b) {
				e = d;
			} else {
				e = b;
			}
		} else if (!(b > a) && (c > a)){
			if (d > c) {
				e = d;
			} else {
				e = c;
			}
			
		} else if (!(b > a) && !(c > a)) {
			if (d > a) {
				e = d;
			} else {
				e = a;
			}
		}
		System.out.println(e);
		sc.close();
		/**
		 * Alternativ Switch Case
		 */
	}
}
